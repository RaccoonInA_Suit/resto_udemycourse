function sliders ({container, slide, nextArrow, prevArrow, totalCounter, currentCounter, wrapper, field}) {
        // Slider vers1


    // const   prevButton = document.querySelector('.offer__slider-prev'),
    //         nextButton = document.querySelector('.offer__slider-next'),
    //         slides = document.querySelectorAll('.offer__slide'),
    //         total = document.querySelector('#total'),
    //         current = document.querySelector('#current');
    
    // let slideIndex = 1;

    // changeSlides(slideIndex);

    // if (slides.length < 10) {
    //     total.textContent = `0${slides.length}`;
    // } else {
    //     total.textContent = slides.length;
    // }
    
    // function changeSlides (n) {
    //     if (n > slides.length) {
    //         slideIndex = 1;
    //     }

    //     if (n < 1) {
    //         slideIndex = slides.length;
    //     }

    //     slides.forEach(item => item.style.display = 'none');

    //     slides[slideIndex - 1].style.display = 'block';

    //     if (slides.length < 10) {
    //         current.textContent = `0${slideIndex}`;
    //     } else {
    //         current.textContent = slideIndex;
    //     }
    // }

    // function plus (n) {
    //     changeSlides(slideIndex +=n);
    // }

    // prevButton.addEventListener('click', () => {
    //     plus(-1);
    // });

    // nextButton.addEventListener('click', () => {
    //     plus(1);
    // });


    // Sliders vers 2


    const   prevButton = document.querySelector(prevArrow),
            nextButton = document.querySelector(nextArrow),
            slider = document.querySelector(container),
            slides = document.querySelectorAll(slide),
            total = document.querySelector(totalCounter),
            current = document.querySelector(currentCounter),
            slidesWrapper = document.querySelector(wrapper),
            slidesField = document.querySelector(field),
            width = window.getComputedStyle(slidesWrapper).width;
    
    let slideIndex = 1;
    let offset = 0;

    if (slides.length < 10) {
        total.textContent = `0${slides.length}`;
        current.textContent = `0${slideIndex}`;
    } else {
        total.textContent = slides.length;
        current.textContent = slideIndex;
    }

    slidesField.style.width = 100 * slides.length + '%';
    slidesField.style.display = 'flex';
    slidesField.style.transition = '0.5s all';

    slidesWrapper.style.overflow = 'hidden';

    slides.forEach(slide => {
        slide.style.width = width;
    });

    slider.style.position = 'relative';

    const dots = document.createElement('ol'),
          indicators = [];       
    dots.classList.add('carousel-indicators');
    slider.append(dots);

    for (let i = 0; i < slides.length; i++) {
        const dot = document.createElement('li');
        dot.setAttribute('data-slide-to', i + 1);
        dot.classList.add('dot');
        if (i == 0) {
            dot.style.opacity = 1;
        }
        dots.append(dot);
        indicators.push(dot);
    }

    function showSlides () {
        if (slides.length < 10) {
            current.textContent = `0${slideIndex}`;
        } else {
            current.textContent = slideIndex;
        }

        indicators.forEach (dot => dot.style.opacity = '.5');
        indicators[slideIndex - 1].style.opacity = 1;
    }

    function deleteNotDigits (str) {
        return +str.replace(/\D/g, '');
    }

    nextButton.addEventListener('click', () => {
        if (offset == deleteNotDigits(width) * (slides.length - 1)) {
            offset = 0;
        } else {
            offset += deleteNotDigits(width);
        }

        if (slideIndex == slides.length) {
            slideIndex = 1;
        } else {
            slideIndex++;
        }

        showSlides();

        slidesField.style.transform = `translateX(-${offset}px)`;
    });

    prevButton.addEventListener('click', () => {
        if (offset == 0) {
            offset = deleteNotDigits(width) * (slides.length - 1);
        } else {
            offset -= deleteNotDigits(width);
        }

        slidesField.style.transform = `translateX(-${offset}px)`;

        if (slideIndex == 1) {
            slideIndex = slides.length;
        } else {
            slideIndex--;
        }

        showSlides();
    });

    indicators.forEach(dot => {
        dot.addEventListener('click', (e) => {
            const slideTo = e.target.getAttribute('data-slide-to');

            slideIndex = slideTo;

            offset = deleteNotDigits(width) * (slideTo - 1);

            slidesField.style.transform = `translateX(-${offset}px)`;
            
            showSlides();
        });
    });
}

export default sliders;